const data = require('./countries.json');

import { createConnection, Connection } from 'typeorm';
import Country from './entity/Country';


async function main() {
    // db connection
    let db: Connection;
    try {
        db = await createConnection();
        console.log('Database connected');
    } catch (err) {
        console.log(err);
        console.log(err, 'SEVERE: TypeORM failed to connect');
        process.exit(1);
    }

    const countryRepository = db.getRepository(Country);

    data.forEach(async (element: any) => {
        if (element == null) {
            return;
        }

        const country = await countryRepository.findOne({ countryCode: element.countryCode, date: element.date });
        console.log(country)
        if (!country) {

            let newCountry = new Country();
            newCountry.countryCode = element.countryCode;
            newCountry.date = element.date;
            newCountry.info = element.info;
            newCountry.gdpAdjusted = element.gdpAdjusted
            countryRepository.save(newCountry);
            // console.log(newCountry)
        }

    });


}


main();