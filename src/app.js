const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');

const csv = require('csv-parser');
var moment = require('moment');
const createCsvWriter = require('csv-writer').createArrayCsvWriter;


//fetch data from website

const fetchHtml = async () => {
    let result;
    await new Promise((resolve, reject) => {

        request('https://www.iatatravelcentre.com/international-travel-document-news/1580226297.htm', (error, response, html) => {
            if (!error && response.statusCode == 200) {
                const $ = cheerio.load(html);
                const siteHTML = $('.middle').html();
                const jsonPart = siteHTML.split('script')[2]
                const extractValues = jsonPart.split('values:')[1]
                const mainValues = extractValues.split("new svgMap")[0]
                const toBePopped = mainValues.trim().split("");
                toBePopped.pop();
                const list = toBePopped.join("")
                // let l = list.split(",");
                result = list.replace(/&#(\d+);/g, function (match, dec) {
                    return String.fromCharCode(dec);
                });
                fs.appendFile('result.txt',
                    result, (err) => {
                        if (err) {
                            console.log('Unable to write to file');
                        }
                    });
                resolve();
            }
        });
    });
    return result
}


// generates the array of object with fields: countryCode, gdp, gdpAdjusted
const getHalfproductArray = (record) => {
    result = [];
    const lineArray = record.split('\n');
    lineArray.shift();
    lineArray.pop();
    lineArray.map((line, lineIndex) => {
        if (lineIndex % 4 === 3) { return }
        const CountryIndex = Math.floor(lineIndex / 4);
        const currCountry = result[CountryIndex];
        if (!currCountry) {
            const countryCode = line.split(': {')[0].trim();
            result[CountryIndex] = { countryCode }
        }
        else {
            const [key, value] = line.split(':');
            if (key && value) {
                currCountry[key.trim()] = value.trim();
                result[CountryIndex] = currCountry;
            } else { console.log(line, '------problem') }
        }
    })
    return result
}


const createUpdatedData = async () => {
    let res = await fetchHtml();

    const halfProduct = getHalfproductArray(res)
    // console.log(halfProduct)

    const product = halfProduct.map(country => {
        const { countryCode, gdp, gdpAdjusted } = country
        if (!(countryCode && gdp && gdpAdjusted)) {
            // console.log(country)
            return
        }

        const gdpLines = gdp && gdp.split('<br/>')
        const firstLine = gdpLines && gdpLines.shift();
        // console.log(firstLine)
        const date = firstLine && firstLine.split('Published')[1].trim();
        const info = gdpLines.join('\n')
        // console.log(gdpAdjusted)
        return { countryCode, date, info, gdpAdjusted }
    })

    // console.log(product)



    fs.appendFile('countries.txt',
        JSON.stringify(product)
        , (err) => {
            if (err) {
                console.log('Unable to write to file');
            }
        });

    // const previousData = await CSV_Reader();
    //prevousData - Data from CSV

    // const countryData = [];
    // res.map(i => {
    //     let country = {};
    //     let [name, nameRest] = i.split('&#32')
    //     country.name = name;
    //     const countryName = country.name;
    // console.log("countryname ==> ", countryName)

    // })
    //     const countryData = [];
    //     res.map(i => {
    //         let country = {}
    //         const [name, nameRest] = i.split("</b>");
    //         country.name = name;
    //         const countryName = country.name;

    //         country.version = '1';
    //         let version = country.version;



    //         const [date, dateRest] = nameRest.split("<br>");
    //         country.date = date.substring(date.length - 10);
    //         const newDate = country.date;


    //         const prevCountryRow = previousData[countryName]

    //         if (prevCountryRow) {
    //             if (prevCountryRow[1] === newDate) {
    //                 countryData.push(prevCountryRow);
    //                 //IF Nothing Changed then leave everything as it is
    //                 return 0;
    //             } else {
    //                 version = ++prevCountryRow[4];
    //             }
    //         }


    //         if (dateRest !== undefined) {

    //             if (dateRest.search("suspended") != "-1") {
    //                 country.suspended = "suspended";
    //             }
    //             else if (dateRest.search("closed") != "-1") {
    //                 country.suspended = "closed"
    //             }
    //             else {
    //                 country.suspended = "N/A"
    //             }

    //             country.text = dateRest.split("<br>").toString()

    //         }
    //         const suspended = country.suspended;
    //         const text = country.text;



    //         countryData.push([countryName, newDate, text, suspended, version])
    //     }
    //     )


    //     return countryData;

    // }




    // //Write in CSV and update
    // const CSV_Writer = async () => {
    //     const res = await createUpdatedData();

    //     const csv_writer = createCsvWriter({
    //         header: ['Country', 'Last Updated', 'Text', 'Status', 'Version'],
    //         path: './project.csv'
    //     })

    //     csv_writer.writeRecords(res)
    //         .then(() => {
    //             console.log('...Done');
    //         });

    // }


    // //CSV reader

    // const CSV_Reader = async () => {

    //     const res = {};

    //     await new Promise((resolve, reject) => {
    //         fs.createReadStream('./project.csv')
    //             .pipe(csv())
    //             .on('data', (data) => {
    //                 const { Country, Text, Status, Version } = data;
    //                 res[Country] = [Country, data['Last Updated'], Text, Status, Version];
    //             })
    //             .on('end', () => {
    //                 console.log('project.csv file successfully read');
    //                 resolve();
    //             });
    //     })

    //     return res;

}



fetchHtml();
createUpdatedData()
// CSV_Writer();
// CSV_Reader();


module.exports = {
    fetchHtml,
    createUpdatedData,
    // CSV_Writer,
    // CSV_Reader
}