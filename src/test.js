const { fetchHtml,
    createUpdatedData,
    CSV_Writer,
    CSV_Reader,
    get_date
} =  require('./app');



test('ITALY has correct country name', async() => {
    const data  = await CSV_Reader()
    expect(await  data.ITALY[0]).toBe('ITALY');
 });



 test('ALBANIA has status "suspended"', async() => {
    const data  = await CSV_Reader()
    expect(await data.ALBANIA[3]).toBe('suspended');
 });


 
 test('BAHAMAS has status "closed"', async() => {
    const data  = await CSV_Reader()
    expect(await data.BAHAMAS[3]).toBe('closed');
 });



test('Row 5 in updated record has 5 fields', async() => {
    const res = await createUpdatedData();
    const rowIndex = 5;
    expect(res[rowIndex].length).toBe(5);
});