import {
    Entity,
    PrimaryGeneratedColumn,
    Column
} from 'typeorm';

@Entity()
export default class Country {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    countryCode: string

    @Column()
    date: string

    @Column()
    info: string

    @Column()
    gdpAdjusted: string


}